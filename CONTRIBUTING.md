# CONTRIBUTING

If you want to contribute in this project then your help and expertise is more than welcome. You just need to be registered GitLab user to request the pull into the repository

### Pull rules

This repository is not about creating the best code but to show multiple configuration examples for docker-compose. Having this in mind if you want to send the pull request please consider following few basic rules:
- If there is an obvious security issue with the proposed deployment file the commit can update existing docker-compose.yml file
- Otherwise the commit should add new file and you should update the README.md with proper description 

It is simple as that :)