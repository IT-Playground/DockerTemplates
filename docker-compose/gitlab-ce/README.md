# GitLab-CE

The docker-compose files for GitLab-CE deployments

### docker-compose.v3.yml
- Version 3
- External network for 'gitlab' container
- Internal network for rest of containers
- Dedicated local volumes for each container
- restart: unless-stopped for all containers
- Postgres database external access on port 35432
- Default project credentials

