# AWX

The docker-compose files for awx deployments

### docker-compose.v3.yml
- Version 3
- External network for 'task' and 'web' containers
- Internal network for rest of containers
- Dedicated local volumes for each container
- restart: unless-stopped for all containers
- Postgres database external access on port 15432
- Default awx credentials

### docker-compose.v2.yml
- Version 2
- External network all containers
- Local datastore path for each container persistent data
- restart: unless-stopped for all containers
- Postgres database external access on port 15432
- Default awx credentials
